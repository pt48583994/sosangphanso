cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(sosangphanso VERSION 50.0 LANGUAGES CXX)
if(NOT DEFINED BIGNUM)
    set(BIGNUM 1)
endif()
if(NOT DEFINED BIGNUM_USE_MPZ)
  set(BIGNUM_USE_MPZ 1)
endif()
configure_file(include/sosangphanso_version.hxx.in include/sosangphanso_version.hxx)
# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()
set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
add_library(sosangphanso SHARED
    src/sosangphanso-src.cxx
    classfunctions/so.cxx
    classfunctions/phanso.cxx
)
find_library(GMP gmp)
find_library(GMPXX gmpxx)
if(BIGNUM_USE_MPZ EQUAL 1)
target_link_libraries(sosangphanso PUBLIC "${GMP}")
target_link_libraries(sosangphanso PUBLIC "${GMPXX}")
endif()
add_executable(sosangphanso-wrapper test/wrapper.cxx)
target_link_libraries(sosangphanso-wrapper PUBLIC sosangphanso)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${CMAKE_CURRENT_BINARY_DIR}/include)
install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/include/sosangphanso.hxx" DESTINATION include)
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/include/sosangphanso_version.hxx" DESTINATION include)

install(TARGETS sosangphanso  DESTINATION lib)
install(TARGETS sosangphanso-wrapper DESTINATION bin)
enable_testing()
add_test(NAME Version COMMAND sosangphanso-wrapper)
set_tests_properties(Version
  PROPERTIES PASS_REGULAR_EXPRESSION "sosangphanso version ${sosangphanso_VERSION_MAJOR}.${sosangphanso_VERSION_MINOR}"
)
unset(BIGNUM CACHE)
unset(BIGNUM_USE_MPZ CACHE)