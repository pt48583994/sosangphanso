#!/bin/bash -e
pushd tmp-dbgsym/build
cmake --install . --prefix ..
popd
pushd pkgs-dbgsym
mkdir sosangphanso-dbgsym/usr
mkdir libsosangphanso-dev-dbgsym/usr
mkdir libsosangphanso-dev-dbgsym/builds
mkdir libsosangphanso-dev-dbgsym/builds/pt48583995
cd libsosangphanso-dev-dbgsym/builds/pt48583995
git clone https://gitlab.com/pt48583995/sosangphanso
cd sosangphanso
rm -rf .git
cd ../../../..
mv ../tmp-dbgsym/bin sosangphanso-dbgsym/usr
mv ../tmp-dbgsym/lib libsosangphanso-dev-dbgsym/usr
mv ../tmp-dbgsym/include libsosangphanso-dev-dbgsym/usr
chmod -R 755 .
dpkg-deb --build --root-owner-group libsosangphanso-dev-dbgsym ../libsosangphanso-dev-dbgsym-$(git rev-parse --short HEAD).deb
dpkg-deb --build --root-owner-group sosangphanso-dbgsym ../sosangphanso-dbgsym-$(git rev-parse --short HEAD).deb
popd