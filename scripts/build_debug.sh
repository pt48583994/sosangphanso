#!/bin/bash -e
mkdir tmp-dbgsym
pushd pkgs-dbgsym
rm -rf sosangphanso-dbgsym/usr libsosangphanso-dev-dbgsym/usr 
cmake  .. -B ../tmp-dbgsym/build -DCMAKE_BUILD_TYPE=Debug
cd ../tmp-dbgsym/build
make
popd