#!/bin/bash -e
pushd tmp/build
cmake --install . --prefix ..
popd
pushd pkgs
mkdir sosangphanso/usr
mkdir libsosangphanso-dev/usr
mv ../tmp/bin sosangphanso/usr
mv ../tmp/lib libsosangphanso-dev/usr
mv ../tmp/include libsosangphanso-dev/usr
chmod -R 755 .
dpkg-deb --build --root-owner-group libsosangphanso-dev ../libsosangphanso-dev-$(git rev-parse --short HEAD).deb
dpkg-deb --build --root-owner-group sosangphanso ../sosangphanso-$(git rev-parse --short HEAD).deb
popd 