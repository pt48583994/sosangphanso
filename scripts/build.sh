#!/bin/bash -e
mkdir tmp
pushd pkgs
rm -rf sosangphanso/usr libsosangphanso-dev/usr 
cmake  .. -B ../tmp/build
cd ../tmp/build
make
popd