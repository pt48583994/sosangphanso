#include <include/private.hxx>
using namespace std;
mpz_class U(mpz_class a, mpz_class b) {
  mpz_class t;
  while (b != 0) {
    t = a % b;
    a = b;
    b = t;
  }
  return a;
}
mpz_class P(mpz_class X, mpz_class Y) {
  mpz_class t = 1;
  for (mpz_class d = 0; d < Y; d++)
    t = t * X;
  return t;
}
void S(string &j, const string &h) {
  size_t p = 0;
  while ((p = j.find(h, p)) != std::string::npos) {
    j.replace(p, h.length(), "");
  }
}
string S_nomod(string t, const string h) {
  string j = t;
  size_t p = 0;
  while ((p = j.find(h, p)) != std::string::npos) {
    j.replace(p, h.length(), "");
  }
  return j;
}
mpz_class s(string X) {
  mpz_class a;
  if (count(X.begin(), X.end(), '-') % 2 == 1) {
    S(X, "-");
    a = 1;
  }
  mpz_class d;
  for (mpz_class ch : X) {
    d = (d * 10) + (ch - '0');
  }
  if (a == 1) {
    d = -d;
  }
  return d;
}
string m(mpz_class v) {
  ostringstream o;
  o << v;
  return o.str();
}
string m_ll(size_t v) {
  ostringstream o;
  o << v;
  return o.str();
}
vector<mpz_class> R(mpz_class A, mpz_class B) {
  vector<mpz_class> x(2, 0);
  x[0] = A / U(A, B);
  x[1] = B / U(A, B);
  return x;
}
vector<mpz_class> AL(string C, string D, string E) {
  mpz_class X, Y, T, Z, K, A, B, L, F, H;
  vector<mpz_class> a(2, 0);
  X = s(m_ll(D.length()));
  Y = s(m_ll(E.length()));
  if (D != "K" && E != "K") {
    A = s(D);
    B = s(E);
    H = s(C);
    T = ((A)*P(10, Y) - (A) + (B));
    Z = (P(10, X + Y) - P(10, X));
    F = (H * Z + T);
    return R(F, Z);
  } else if (D == "K" && E != "K") {
    B = s(E);
    A = s(C);
    L = (P(10, Y) - 1);
    K = (A * L + B);
    return R(K, L);
  } else if (D != "K" && E == "K") {
    A = s(D);
    B = s(C);
    L = (P(10, X));
    K = (B * L + A);
    return R(K, L);
  } else {
    return R(s(C), 1);
  }
}
vector<string> IP(string E) {
  size_t K, GCC, HC;
  string A, B, J, F, L, bk;
  vector<string> x(3, "0");
  J = E[0];
  K = E.find("(");
  GCC = E.find(")");
  if (K == std::string::npos && GCC == std::string::npos) {
    B = "(K)";
  } else {
    B = E.substr(K);
  }
  HC = E.find(".");
  if (HC == std::string::npos) {
    A = ".K";
  } else {
    if (B == "(K)") {
      A = E.substr(HC);
    } else {
      S(E, B);
      A = E.substr(HC);
    }
  }
  S(E, A);
  S(A, ".");
  S(B, "(");
  S(B, ")");
  if (J == "-") {
    S(E, "-");
  }
  x[0] = E;
  x[1] = A;
  x[2] = B;
  return x;
}
string sosangphanso(string bk) {
  mpz_class T, D;
  size_t J;
  vector<string> x = IP(bk), f(3, "");
  vector<mpz_class> y = AL(x[0], x[1], x[2]);
  J = count(bk.begin(), bk.end(), '-');
  if (J % 2 == 1) {
    f[1].append("-");
  }
  f[1].append(m(y[0]));
  f[2] = m(y[1]);
  f[0].append(f[1]);
  if (f[2] != "1") {
    f[0].append("/");
    f[0].append(f[2]);
  }
  return f[0];
}
vector<mpz_class> checkifonly2or5infactors(string b) {
  mpz_class a = s(b);
  vector<mpz_class> c(3, 0);
  while (a % 5 == 0) {
    a = a / 5;
    c[2] = c[2] + 1;
  }
  while (a % 2 == 0) {
    a = a / 2;
    c[1] = c[1] + 1;
  }
  if (a != 1) {
    c[0] = 1;
  }
  return c;
}
mpz_class max(mpz_class a, mpz_class b) {
  if (a > b)
    return a;
  else
    return b;
}
string phanso_raw(string a) {
  string tuso, mauso;
  size_t b;
  b = a.find("/");
  mauso = a.substr(b);
  S(a, mauso);
  S(mauso, "/");
  tuso = a;
  vector<mpz_class> l = R(s(tuso), s(mauso));
  tuso = m(l[0]);
  mauso = m(l[1]);
  mpz_class c = max(checkifonly2or5infactors(mauso)[1],
                    checkifonly2or5infactors(mauso)[2]);
  mpz_class d, AAA = P(10, c), TMD = 10;
  string t=m(AAA),F;
  if (AAA % s(mauso) == 0) {
    F="";
    F.append(m(s(tuso) * s(t) / s(mauso)));
    F.append("/");
    F.append(t);
    return F;
  } else {
    while (d != 1) {
      if (((TMD - 1) * AAA) % s(mauso) == 0) {
        d = 1;
        t = m(((TMD - 1) * AAA));
      }
      TMD = TMD * 10;
    }
    F="";
    F.append(m(s(tuso) * s(t) / s(mauso)));
    F.append("/");
    F.append(t);
    return F;
  }
}
vector<string> a2(string reversed_reduce_from_sosangphanso_phanso) {
  string tuso, mauso;
  size_t b;
  vector<string> f(3, "");
  b = reversed_reduce_from_sosangphanso_phanso.find("/");
  mauso = reversed_reduce_from_sosangphanso_phanso.substr(b);
  S(reversed_reduce_from_sosangphanso_phanso, mauso);
  S(mauso, "/");
  tuso = reversed_reduce_from_sosangphanso_phanso;
  mpz_class T = s(m_ll(count(tuso.begin(), tuso.end(), '-')));
  mpz_class T2 = s(m_ll(count(mauso.begin(), mauso.end(), '-')));
  S(tuso, "-");
  S(mauso, "-");
  f[0] = m((s(tuso) - (s(tuso) % s(mauso))) / s(mauso));
  tuso = m(s(tuso) % s(mauso));
  size_t x = count(mauso.begin(), mauso.end(), '0');
  size_t n = count(mauso.begin(), mauso.end(), '9');
  mpz_class C;
  if (n != 0) {
    C = (P(10, s(m_ll(n))) - 1);
  } else {
    C = 1;
  }
  mpz_class B = (s(tuso) % C);
  mpz_class A = (s(tuso) - B) / C;
  if (n != 0 && tuso != "0") {
    string F;
    F.append("(");
    for (mpz_class t = 0; t < s(m_ll(n - (m(s(tuso) % C).length()))); t++)
      F.append("0");
    F.append(m(s(tuso) % (P(10, s(m_ll(n))) - 1)));
    F.append(")");
    f[2] = F;
  }
  if (x != 0 && tuso != "0") {
    string F;
    F.append(".");
    for (mpz_class y = 0; y < s(m_ll(x - (m(A).length()))); y++)
      F.append("0");
    F.append(m(A));
    f[1] = F;
  } else if (f[2] != "") {
    f[1] = ".";
  } else {
    f[1] = "";
  }
  if ((T + T2) % 2 != 0) {
    string tmp = f[0];
    f[0] = "-";
    f[0].append(tmp);
  }
  return f;
}
string phansosangso(string phanso) {
  string F;
  vector<string> A = a2(phanso_raw(phanso));
  F.append(A[0]);
  F.append(A[1]);
  F.append(A[2]);
  return F;
}
