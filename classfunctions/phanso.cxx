#include <include/private.hxx>
using namespace std;
phanso phanso::reduce() {
  bignum_backend gcd = gcda(tuso, mauso);
  tuso /= gcd;
  mauso /= gcd;
  if (mauso < 0) {
    tuso = -tuso;
    mauso = -mauso;
  }
  return (*this);
}
vector<bignum_backend> phanso::get_base() { return {tuso, mauso}; }
string phanso::to_str() {
  ostringstream a;
  a << (*this);
  return a.str();
}
so phanso::to_so() {
  so a = phansosangso({this->tuso, this->mauso});
  return a;
}
phanso phanso::operator+(const phanso &param) {
  phanso tmp;
  tmp.tuso = param.tuso * mauso + tuso * param.mauso;
  tmp.mauso = param.mauso * mauso;
  return tmp;
}
phanso phanso::operator-(const phanso &param) {
  phanso tmp;
  tmp.tuso = tuso * param.mauso - param.tuso * mauso;
  tmp.mauso = param.mauso * mauso;
  return tmp;
}
phanso phanso::operator*(const phanso &param) {
  phanso tmp;
  tmp.tuso = mauso * param.mauso;
  return tmp;
}
phanso phanso::operator/(const phanso &param) {
  phanso tmp;
  tmp.tuso = tuso * param.tuso;
  tmp.mauso = mauso * param.mauso;
  return tmp;
}
ostream &operator<<(ostream &os, const phanso &param) {
  phanso x = param;
  x.reduce();
  os << x.tuso;
  if (x.mauso != 1)
    os << "/" << x.mauso;
  return os;
}
istream &operator>>(istream &is, phanso &param) {
  string combined;
  is >> combined;
  param = combined;
  return is;
}
phanso::phanso() {
  tuso = 0;
  mauso = 1;
}
phanso::phanso(string param) {
  string a = param.substr(0, param.find("/")),
         b = param.find("/") == string::npos
                 ? "1"
                 : param.substr(param.find("/") + 1,
                                param.size() - param.find("/") - 1);
  tuso = s2m(a);
  mauso = s2m(b);
}
phanso::phanso(vector<bignum_backend> param) {
  tuso = param[0];
  mauso = param[1];
}
#if __cplusplus > 201703L
int phanso::operator<=>(const phanso &param) const {
  bignum_backend tuso1 = tuso * param.mauso;
  bignum_backend tuso2 = param.tuso * mauso;
  if (tuso1 > tuso2) {
    return 1;
  } else if (tuso1 == tuso2) {
    return 0;
  } else {
    return -1;
  }
}
#else
bool phanso::operator==(const phanso &param) const {
  bignum_backend tuso1 = tuso * param.mauso;
  bignum_backend tuso2 = param.tuso * mauso;
  if (tuso1 == tuso2) {
    return 1;
  } else {
    return 0;
  }
}
bool phanso::operator!=(const phanso &param) const {
  bignum_backend tuso1 = tuso * param.mauso;
  bignum_backend tuso2 = param.tuso * mauso;
  if (tuso1 == tuso2) {
    return 1;
  } else {
    return 0;
  }
}
bool phanso::operator<(const phanso &param) const {
  bignum_backend tuso1 = tuso * param.mauso;
  bignum_backend tuso2 = param.tuso * mauso;
  if (tuso1 < tuso2) {
    return 1;
  } else {
    return 0;
  }
}
bool phanso::operator>(const phanso &param) const {
  bignum_backend tuso1 = tuso * param.mauso;
  bignum_backend tuso2 = param.tuso * mauso;
  if (tuso1 > tuso2) {
    return 1;
  } else {
    return 0;
  }
}
bool phanso::operator<=(const phanso &param) const {
  return ((*this) < param || (*this) == param);
}
bool phanso::operator>=(const phanso &param) const {
  return ((*this) > param || (*this) == param);
}
#endif