#include "../include/private.hxx"
using namespace std;
bignum_backend pow_log(bignum_backend a, bignum_backend b) {
  if (b == 0) {
    return 1;
  } else {
    bignum_backend half = pow_log(a, b / 2);
    if (b % 2 == 1) {
      return half * half * a;
    } else {
      return half * half;
    }
  }
}
bignum_backend gcda(bignum_backend x, bignum_backend y) {
  if (y == 0) {
    return x;
  } else {
    return gcda(y, x % y);
  }
}
bignum_backend s2m(string x) {
  bignum_backend tmp = 1;
  if (x[0] == '-') {
    tmp = -1;
    x = x.substr(1, x.size() - 1);
  }
  bignum_backend d = 0;
  for (bignum_backend digit : x) {
    d *= 10;
    d += digit - 48;
  }
  return tmp * d;
}
string m2s(bignum_backend x) {
  ostringstream o;
  o << x;
  return o.str();
}
string filler(string x) {
  size_t d = x.find("."), e = x.find("(");
  if (d == UINT64_MAX) {
    x += ".0(0)";
  } else {
    if (e == UINT64_MAX) {
      x += "(0)";
    }
  }
  return x;
}
vector<string> parse_so(string x) {
  vector<string> a;
  x = filler(x);
  size_t d = x.find("."), e = x.find("(");
  a.push_back(x.substr(0, d));
  a.push_back(x.substr(d + 1, e - d - 1));
  a.push_back(x.substr(e + 1, x.size() - e - 2));
  return a;
}
vector<bignum_backend> sosangphanso(vector<string> x) {
  size_t a = x[1].size(), b = x[2].size();
  vector<bignum_backend> dn(2, 0);
  dn[1] = pow_log(10, a) * (pow_log(10, b) - 1);
  dn[0] = s2m(x[0]) * dn[1] + s2m(x[1]) * (dn[1] / pow_log(10, a)) + s2m(x[2]);
  return dn;
}
vector<bignum_backend> ck2or5(bignum_backend a) {
  vector<bignum_backend> c(3, 0);
  while (a % 5 == 0) {
    a = a / 5;
    c[2] = c[2] + 1;
  }
  while (a % 2 == 0) {
    a = a / 2;
    c[1] = c[1] + 1;
  }
  c[0] = a;
  return c;
}
vector<string> phansosangso(vector<bignum_backend> x) {
  bignum_backend a1 = x[0], b = x[1], a;
  bignum_backend gab = gcda(a1, b);
  a = (a1 % b) / gab;
  b = b / gab;
  vector<bignum_backend> c = ck2or5(b);
  bignum_backend d = pow_log(10, max(c[1], c[2]));
  bignum_backend s = c[0], r = d;
  bignum_backend k = max(c[1], c[2]), l = 0;
  if (r % s != 0) {
    r = 9 * d;
    l++;
  }
  while (r % s != 0) {
    r = r * 10;
    r = r + d * 9;
    l++;
  }
  bignum_backend j = r / d;
  bignum_backend f = (r * a) / b;
  bignum_backend e = f / j;
  bignum_backend t = f % j;
  string m = m2s(e), n = m2s(t);
  reverse(m.begin(), m.end());
  reverse(n.begin(), n.end());
  bignum_backend r1 = m.size(), r2 = n.size();
  for (bignum_backend i = 0; i < (bignum_backend)(k - r1); i++) {
    m += '0';
  }
  for (bignum_backend i = 0; i < (bignum_backend)(l - r2); i++) {
    n += '0';
  }
  reverse(m.begin(), m.end());
  reverse(n.begin(), n.end());
  vector<string> v;
  v.push_back(m2s(a / b));
  v.push_back(m);
  v.push_back(n);
  return v;
}
