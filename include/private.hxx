#include "sosangphanso.hxx"
#ifndef LIBSOSANGPHANSO_PRIVATE_HXX_INCLUDED
#define LIBSOSANGPHANSO_PRIVATE_HXX_INCLUDED
bignum_backend s2m(std::string);
std::string m2s(bignum_backend);
bignum_backend gcda(bignum_backend, bignum_backend);
std::vector<std::string> parse_so(std::string);
std::vector<bignum_backend> sosangphanso(std::vector<std::string>);
std::vector<std::string> phansosangso(std::vector<bignum_backend>);
#endif /* LIBSOSANGPHANSO_PRIVATE_HXX_INCLUDED */
